package tokenmanager

import (
	"fmt"
	"time"
)

func (tm *TokenManager) ValidateToken(tokenType int, token string, s string, optFns ...WithGetTokenFn) bool {
	options := GetTokenOpts{}
	for _, fn := range optFns {
		fn(&options)
	}

	if tm.StaticToken != nil {
		return token == *tm.StaticToken
	}

	now := time.Now()
	if options.AtTime != nil {
		now = *options.AtTime
	} else if tm.AtTime != nil {
		now = *tm.AtTime
	}

	// Current time and oldest valid time
	unixNow := now.Truncate(*tm.GenerationInterval).Unix()
	oldestValidTime := unixNow - int64(tm.TimeLimit.Seconds())

	//log.Printf("start: %d, end: %d\n", oldestValidTime, unixNow)
	// Check each 5-second interval from now back to the oldest valid time.
	for ts := unixNow; ts >= oldestValidTime; ts -= 5 {
		toHash := fmt.Sprintf("%s:%s:%d:%d", *tm.Secret, s, tokenType, ts)
		generatedToken := tokenFromString(toHash, tm.CharacterSet, tm.TokenLength)

		//log.Printf("checking %s against %s at %d\n", token, generatedToken, ts)
		if token == generatedToken {
			return true
		}
	}

	// If no match was found in the entire interval, validation fails.
	return false
}
