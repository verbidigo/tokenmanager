package tokenmanager

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

const (
	TokenTypeForgotPassword = 1
	TokenTypeLogin          = 2
	TestSecret              = "MyTestingSecret"
)

func TestUserTokenGood(t *testing.T) {
	tm, err := New(WithSecret(TestSecret))
	if !assert.NoError(t, err) {
		return
	}
	for x := 0; x < 100; x++ {
		e := fmt.Sprintf("user+%d@test.com", x)
		tok := tm.GenerateToken(TokenTypeForgotPassword, e)
		ok := tm.ValidateToken(TokenTypeForgotPassword, tok, e)
		if !assert.True(t, ok, "email: %s / token: %s", e, tok) {
			return
		}
	}
}

func TestUserTokenDifferentEmailBad(t *testing.T) {
	tm, err := New(WithSecret(TestSecret))
	if !assert.NoError(t, err) {
		return
	}
	for x := 0; x < 100; x++ {
		e1 := fmt.Sprintf("user1+%d@test.com", x)
		e2 := fmt.Sprintf("user2+%d@test.com", x)
		tok := tm.GenerateToken(TokenTypeForgotPassword, e1)
		ok := tm.ValidateToken(TokenTypeForgotPassword, tok, e2)
		if !assert.False(t, ok, "email: %s -- %s / token: %s", e1, e2, tok) {
			return
		}
	}
}

func TestUserTokenDifferentTypeBad(t *testing.T) {
	tm, err := New(WithSecret(TestSecret))
	if !assert.NoError(t, err) {
		return
	}
	for x := 0; x < 100; x++ {
		e1 := fmt.Sprintf("user1+%d@test.com", x)
		e2 := fmt.Sprintf("user1+%d@test.com", x)
		tok := tm.GenerateToken(TokenTypeForgotPassword, e1)
		ok := tm.ValidateToken(TokenTypeLogin, tok, e2)
		if !assert.False(t, ok, "email: %s -- %s / token: %s", e1, e2, tok) {
			return
		}
	}
}

func TestUserTokenExpired(t *testing.T) {
	tm, err := New(WithSecret(TestSecret))
	if !assert.NoError(t, err) {
		return
	}
	nowTime := time.Now().Add(-1 * (DefaultTokenAgeMinutes + 1) * time.Minute)
	for x := 0; x < 100; x++ {
		e := fmt.Sprintf("user1+%d@test.com", x)
		tok := tm.GenerateToken(TokenTypeForgotPassword, e, WithGetTokenAtTime(nowTime))
		ok := tm.ValidateToken(TokenTypeLogin, tok, e)
		if !assert.False(t, ok, "email: %s / token: %s", e, tok) {
			return
		}
	}
}

func TestValidateWithStaticToken(t *testing.T) {
	const StaticToken = "987654"
	tm, err := New(WithSecret(TestSecret), WithStaticToken(StaticToken))
	if !assert.NoError(t, err) {
		return
	}
	for x := 0; x < 100; x++ {
		e := fmt.Sprintf("user+%d@test.com", x)
		tok := tm.GenerateToken(TokenTypeForgotPassword, e)
		if !assert.Equal(t, StaticToken, tok) {
			return
		}
		ok := tm.ValidateToken(TokenTypeForgotPassword, tok, e)
		if !assert.True(t, ok, "email: %s / token: %s", e, tok) {
			return
		}
	}
}
