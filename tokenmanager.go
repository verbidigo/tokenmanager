package tokenmanager

import (
	"errors"
	"time"
)

const DefaultTokenAgeMinutes = 15
const DefaultTokenGenerationIntervalSeconds = 60
const DefaultTokenLength = 8
const DefaultCharacterSet = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789" // avoiding 0, 1, I, O, L, and B to reduce the chance of misreading.

type TokenManager struct {
	TimeLimit          *time.Duration
	GenerationInterval *time.Duration
	CharacterSet       string
	TokenLength        int
	Secret             *string
	AtTime             *time.Time
	StaticToken        *string
}

// WithTokenOptions is a function that can be passed to New to modify the token manager
type WithTokenOptions func(*TokenManager)

// WithTokenTimeLimit sets the time limit for a token to be valid. If not set, the default is 15 minutes
func WithTokenTimeLimit(t time.Duration) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.TimeLimit = &t
	}
}

// WithTokenGenerationInterval sets the interval at which tokens are generated. If not set, the default is 60 seconds
func WithTokenGenerationInterval(t time.Duration) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.GenerationInterval = &t
	}
}

// WithCharacterSet sets the character set that will be used to generate tokens. If not set, the default is the
// english alphabet minus the characters 0, 1, I, O, L, and B
func WithCharacterSet(s string) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.CharacterSet = s
	}
}

// WithTokenLength sets the length of the token. If not set, the default is 8
func WithTokenLength(l int) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.TokenLength = l
	}
}

// WithSecret sets the secret that will be used to generate tokens. This is required
func WithSecret(s string) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.Secret = &s
	}
}

// WithAtTime sets the time that the token will be generated at. If not set, the current time will be used
func WithAtTime(t time.Time) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.AtTime = &t
	}
}

// WithStaticToken sets a static token that will be returned instead of a generated token. Used for testing
func WithStaticToken(t string) WithTokenOptions {
	return func(opts *TokenManager) {
		opts.StaticToken = &t
	}
}

func New(opts ...WithTokenOptions) (*TokenManager, error) {
	tm := TokenManager{}
	for _, opt := range opts {
		opt(&tm)
	}

	if tm.Secret == nil {
		return nil, errors.New("secret is required")
	}

	// set defaults if we need to
	if tm.TimeLimit == nil {
		d := time.Minute * DefaultTokenAgeMinutes
		tm.TimeLimit = &d
	}

	if tm.GenerationInterval == nil {
		d := time.Second * DefaultTokenGenerationIntervalSeconds
		tm.GenerationInterval = &d
	}

	if tm.CharacterSet == "" {
		tm.CharacterSet = DefaultCharacterSet
	}

	if tm.TokenLength == 0 {
		tm.TokenLength = DefaultTokenLength
	}

	return &tm, nil
}
