package tokenmanager

import (
	"fmt"
	"time"
)

type GetTokenOpts struct {
	AtTime *time.Time
}

// WithGetTokenFn is a function that can be passed to GenerateToken to modify the token generation
type WithGetTokenFn func(*GetTokenOpts)

// WithGetTokenAtTime sets the time that the token will be generated at. If not set, the current time will be used
func WithGetTokenAtTime(t time.Time) WithGetTokenFn {
	return func(opts *GetTokenOpts) {
		opts.AtTime = &t
	}
}

// GenerateToken gets a token for a given tokenType and string. TokenType is used simply as a prefix to the
// token to allow the same string to be used for different purposes and return different tokens
func (tm *TokenManager) GenerateToken(tokenType int, s string, optFns ...WithGetTokenFn) string {
	options := GetTokenOpts{}
	for _, fn := range optFns {
		fn(&options)
	}

	if tm.StaticToken != nil {
		return *tm.StaticToken
	}

	now := time.Now()
	if options.AtTime != nil {
		now = *options.AtTime
	} else if tm.AtTime != nil {
		now = *tm.AtTime
	}

	roundedTimestamp := now.Truncate(*tm.GenerationInterval).Unix()
	toHash := fmt.Sprintf("%s:%s:%d:%d", *tm.Secret, s, tokenType, roundedTimestamp)
	return tokenFromString(toHash, tm.CharacterSet, tm.TokenLength)
}
