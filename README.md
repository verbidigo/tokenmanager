# Token Manager

Token Manager provides calculated one-time-password tokens that can be used for a variety of purposes.

## Examples

Review the `validate_test.go` file for usage examples
