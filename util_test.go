package tokenmanager

import (
	"crypto/rand"
	"github.com/stretchr/testify/assert"
	mrand "math/rand"
	"testing"
	"time"
)

const DefaultTestSecret = "MyTestSecret1"

func TestTokenFromString(t *testing.T) {
	tm, err := New(WithSecret(DefaultTestSecret))

	if !assert.NoError(t, err) {
		return
	}
	tStrings := []string{
		"test1",
		"test2",
		"test3",
	}

	for _, tString := range tStrings {
		_ = tm.GenerateToken(1, tString)
	}
}

func TestTokenWithDifferentSecrets(t *testing.T) {
	tm1, err := New(WithSecret(DefaultTestSecret))
	now := time.Now()
	if !assert.NoError(t, err) {
		return
	}

	tm2, err := New(WithSecret(DefaultTestSecret + "1"))
	if !assert.NoError(t, err) {
		return
	}

	const tString = "MyTestString"
	tok1 := tm1.GenerateToken(1, tString, WithGetTokenAtTime(now))
	tok2 := tm2.GenerateToken(1, tString, WithGetTokenAtTime(now))
	assert.NotEqual(t, tok1, tok2)
}
func generateRandomString(length int) (string, error) {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()"
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	for i := range b {
		b[i] = charset[b[i]%byte(len(charset))]
	}
	return string(b), nil
}

func randomInt(min, max int) int {
	return min + mrand.Intn(max-min+1)
}

func TestTokenDistributionRoughlyEqual(t *testing.T) {
	now, err := time.Parse(time.RFC3339, "2023-11-21T00:00:00Z")
	if !assert.NoError(t, err) {
		return
	}
	tm, err := New(WithSecret(DefaultTestSecret))
	if !assert.NoError(t, err) {
		return
	}

	occurrences := make(map[rune]int)
	for x := 0; x < 100_000; x++ {
		s, err := generateRandomString(randomInt(8, 24))
		if !assert.NoError(t, err) {
			return
		}

		tok := tm.GenerateToken(1, s, WithGetTokenAtTime(now))
		for _, ch := range tok {
			occurrences[ch]++
		}

	}
	//d, _ := json.MarshalIndent(occurrences, "", "  ")
	//fmt.Println(string(d))

	// Assert we used all characters
	if !assert.Equal(t, len(tm.CharacterSet), len(occurrences)) {
		return
	}
	// Assert that the distribution of characters is roughly equal
	avgOccurrences := 0
	for _, count := range occurrences {
		avgOccurrences += count
	}
	avgOccurrences /= len(occurrences)

	tolerance := int(float64(avgOccurrences) * 0.1) // example tolerance of 10%
	for _, count := range occurrences {
		// Check if each count is within a certain range of the average
		// Adjust the tolerance as needed
		if count < avgOccurrences-tolerance || count > avgOccurrences+tolerance {
			t.Errorf("Distribution of character occurrences is not even")
			return
		}
	}
}
