package tokenmanager

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"strings"
)

func tokenFromStringOld(s string) string {
	hasher := sha256.New()
	hasher.Write([]byte(s))
	hash := strings.ToUpper(hex.EncodeToString(hasher.Sum(nil)))

	numToken := uint64(0)
	for i := 0; i < len(hash); i++ {
		ch := int64(hash[i])
		numToken = numToken << 1
		numToken ^= uint64(ch)
	}
	token := fmt.Sprintf("%06d", numToken%1000000)

	return token
}

func tokenFromStringOld2(s string, charSet string, tokenLength int) string {
	hasher := sha256.New()
	hasher.Write([]byte(s))
	hash := strings.ToUpper(hex.EncodeToString(hasher.Sum(nil)))

	// If the requested token length is longer than the hash, limit it to the hash length
	if tokenLength > len(hash) {
		tokenLength = len(hash)
	}

	// Map the hash to the characters in charSet
	var tokenBuilder strings.Builder
	charSetLen := len(charSet)
	for i := 0; i < tokenLength; i++ {
		charIndex := int(hash[i]) % charSetLen
		tokenBuilder.WriteByte(charSet[charIndex])
	}
	token := tokenBuilder.String()

	return token
}

func tokenFromString(s string, charSet string, tokenLength int) string {
	hasher := sha256.New()
	hasher.Write([]byte(s))
	hash := hasher.Sum(nil) // getting the byte slice directly

	var tokenBuilder strings.Builder
	charSetLen := len(charSet)
	hashLen := len(hash)

	// Use chunks of the hash to determine each character in the token
	for i := 0; i < tokenLength; i++ {
		// Calculate indices, wrapping around the hash with modular arithmetic
		startIdx := (i * 4) % hashLen
		endIdx := (startIdx + 4) % hashLen

		// If the end index is less than the start index, wrap around the hash
		var chunk []byte
		if endIdx > startIdx {
			chunk = hash[startIdx:endIdx]
		} else {
			// Wrap around by concatenating the end and start of the hash slice
			chunk = append(hash[startIdx:], hash[:endIdx]...)
		}

		// Convert the chunk into an unsigned integer
		var num uint32
		if len(chunk) >= 4 {
			num = binary.BigEndian.Uint32(chunk)
		} else {
			// If the chunk is less than 4 bytes, pad it with zeroes
			paddedChunk := make([]byte, 4)
			copy(paddedChunk, chunk)
			num = binary.BigEndian.Uint32(paddedChunk)
		}

		// Use modulo to map the number to a character in the character set
		charIndex := num % uint32(charSetLen)
		tokenBuilder.WriteByte(charSet[charIndex])
	}

	return tokenBuilder.String()
}
